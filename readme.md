# Easy Lancer - API

## Link: https://cap-cauan-t7.herokuapp.com

## Requesters:

### Cadastrar solicitante de serviços

**POST** /requester/signup

Exemplo do corpo da requisição:

```json 
{
  "name": "seu nome",
  "birthdate": "01/02/2001",
  "email": "seuemail@gmail.com",
  "password": "senha"
}
```

Exemplo de retorno:

```json
{
  "id": 6,
  "name": "seu nome",
  "birthdate": "Mon, 02 jan 2001 00:00:00 GMT",
  "email": "seuemail@gmail.com"
}
```

#

### Login do solicitante

**POST** /requester/login

Exemplo do corpo da requisição:

```json
{
  "email": "seuemail@gmail.com", 
  "password": "senha"
}
```

Retorna um token de autenticação no padrão Bearer token.

#

### Lista de todos os solicitantes

**GET** /requester/list

Retorna uma lista com todos os solicitantes.

#

### Visualisar as informações de sua conta como solicitante

##### *requer autenticação*

**GET**/requester

Exemplo de retorno:

```json
{
  "id": 6,
  "name": "seu nome",
  "birthdate": "Mon, 02 jan 2001 00:00:00 GMT",
  "email": "seuemail@gmail.com"
}
```
#

### Auterar dados da sua conta

**PATCH** /requester

##### *requer autenticação*

Exemplo de corpo da requisição:
```json
{
  "name": "ian",
  "birthdate": "10/18/2002",
  "password": "123456"
}
```

Este endpoint não aceita que altere o "email".

#

### Deletar conta

**DELETE** /requester

##### *requer autenticação*

Permite deletar sua conta.

Se o delete for bem sucedido não retorna nada.

****

## Freelancers:

### Cadastrar freelancer

**POST** /freelancer/signup

Exemplo do corpo da requisição:

```json 
{
  "name": "seu nome",
  "birthdate": "01/02/2001",
  "email": "seuemail@gmail.com",
  "about": "Não tenho nada a declarar",
	"categories_id":1,
  "password": "senha"
}
```

Exemplo de retorno:

```json
{
  "id": 6,
  "name": "seu nome",
  "about": "Não tenho nada a declarar",
  "birthdate": "Mon, 02 jan 2001 00:00:00 GMT",
  "email": "seuemail@gmail.com",
  "categories": {
    "id": 1,
    "types": "Dev fromt"
  }
}
```

#

### Login do freelancer

**POST** /freelancer/login

Exemplo do corpo da requisição:

```json
{
  "email": "seuemail@gmail.com", 
  "password": "senha"
}
```

Retorna um token de autenticação no padrão Bearer token.

#

### Lista de todos os freelancer

**GET** /freelancer/list

Retorna uma lista com todos os freelancer.

#

### Visualisar as informações de sua conta como freelancer

##### *requer autenticação*

**GET**/freelancer

Exemplo de retorno:

```json
{
  "id": 4,
  "name": "seu nome",
  "birthdate": "Mon, 02 jan 2001 00:00:00 GMT",
  "email": "seuemail@gmail.com",
  "about": "Não tenho nada a declarar",
  "categories": {
    "id": 1,
    "types": "Dev fromt"
  }
}
```

#

### Auterar dados da sua conta

**PATCH** /freelancer

##### *requer autenticação*

Exemplo de corpo da requisição:

```json
{
  "name": "ian",
  "birthdate": "10/18/2002",
  "password": "123456"
}
```

Este endpoint não aceita que altere o "email".

#

### Deletar conta

**DELETE** /freelancer

##### *requer autenticação*

Permite deletar sua conta.

Se o delete for bem sucedido não retorna nada.
****

## Services:

### Criação de um serviço

##### *requer autenticação com uma conta de solicitante*

**POST** /service

Exemplo de corpo da requisição:

```json
{
  "description": "api kenziehob",
  "budget": 200.50,
  "service_date": "09/16/2021",
  "categories_id": 1
}
```

Exemplo de retorno:

```json
{
  "id": 15,
  "description": "api kenziehob 125",
  "service_date": "Thu, 16 Sep 2021 00:00:00 GMT",
  "budget": 200.5,
  "rating": null,
  "requester_id": 3,
  "status_id": 1,
  "chosen_freelancer_id": null,
  "status": {
    "status_type": "Open"
  },
  "categories": {
    "id": 1,
    "types": "Dev fromt"
  }
}
```

#

### Lista os servisos

**GET** /service

##### *requer autenticação* 

De padrão esta rota retorna uma lista com todos os serviços, mas também aceita como argumento na url parâmetros para filtrar-los

```python

# Sugestão de keys para parâmetros do filtro

[
  {
    "status_id" = { 
      1 = "Open",
      2 = "On progress",
      3 = "Completed"
    }  
  },
  "chosen_freelancer_id",
  "requester_id",
  "categories_id"
]

   


```
#

### Serviço especifico

**GET** /service/<service_id>

##### *requer autenticação* 

Exemplo de retorno:

```json
{
  "id": 3,
  "description": "api kenziehob ",
  "service_date": "Thu, 16 Sep 2021 00:00:00 GMT",
  "budget": 5000.55,
  "rating": null,
  "requester_id": 1,
  "status_id": 1,
  "chosen_freelancer_id": null,
  "status": {
    "status_type": "On progress"
  },
  "categories": {
    "id": 1,
    "types": "Dev fromt"
  }
}
```

#

### Alterar dados do serviço

**PATCH** /service/<service_id>

##### *requer autenticação do dono do serviço*

Exemplo de corpo da requisição:

```json
{
  "description": "api kenziehob 125",
  "service_date": "Thu, 16 Sep 2021 00:00:00 GMT",
  "budget": 12345.88,
  "rating": null,
  "chosen_freelancer_id": 4
  
}
```

Este endpoint não aceita que altere o "requester_id".

Exemplo de returno:

```json
{
  "id": 3,
  "description": "api kenziehob 125",
  "service_date": "Thu, 16 Sep 202100:00:00 GMT",
  "budget": 12345.88,
  "rating": null,
  "requester_id": 1,
  "status_id": 2,
  "chosen_freelancer_id": 4,
  "status": {
    "status_type": "On progress"
  },
  "categories": {
    "id": 1,
    "types": "Dev fromt"
  }
}
```

#

### Deletar conta

**DELETE** /service/<service_id>

##### *requer autenticação do dono do serviço*

Permite deletar seu serviço.

Se o delete for bem sucedido não retorna nada.

#

### Freelancer pode se oferecer a um serviço especifico 

**POST** /service/candidate/<service_id>

##### *requer autenticação de uma conta freelancer*

Se a requisição ser bem sucedida retorna:

```json
{
  "msg": "application made!"
}
```

#

### Listar todos os candidatos para o serviço especifico

**GET** /service/candidates/<service_id>

##### *requer autenticação* 

Exemplo de retorno:

```json
[
  {
    "id": 4,
    "name": "ian",
    "birthdate": "Mon, 16 Sep 2002 00:00:00 GMT",
    "email": "ian5@gmail.com",
    "about": "Não tenho nada a declarar",
    "categories": {
      "id": 1,
      "types": "Dev fromt"
    }
  },
  {
    "id": 2,
    "name": "ian",
    "birthdate": "Mon, 16 Sep 2002 00:00:00 GMT",
    "email": "ian6@gmail.com",
    "about": "Não tenho nada a declarar",
    "categories": {
      "id": 1,
      "types": "Dev fromt"
    }
  }
]
```

****

## Categories:

### Criar novas categories

**POST** /categories/create

##### *requer autenticação* 

Exemplo de corpo da requisição:

```json
{
  "types": "Market"
}
```

Exemplo de retorno:

```json
{
  "id":3,
  "types": "Market"
}
```

#

### Listar todos as categories

**GET** /categories/list

Exemplo de retorno:

```json
[
  {
    "id": 1,
    "types": "Dev fromt"
  },
  {
    "id": 2,
    "types": "Dev back"
  },
  {
    "id": 3,
    "types": "Markte"
  }
]
```


