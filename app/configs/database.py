from flask import Flask
from flask_sqlalchemy import SQLAlchemy

db = SQLAlchemy()


def init_app(app: Flask):
    db.init_app(app)
    app.db = db

    from app.models.categories_model import CategoriesModel
    from app.models.freelancer_model import FreelancerModel
    from app.models.requester_model import RequesterModel
    from app.models.candidates_model import CandidatesModel
    from app.models.service_model import ServiceModel
    from app.models.comments_model import CommentsModel
    from app.models.status_service_model import StatusServiceModel
    
