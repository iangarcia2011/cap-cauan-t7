from flask import Flask
from app.configs import database, migrations, env_configs, jwt
from app.blueprints import init_blueprints

def create_app():
    app = Flask(__name__)

    env_configs.init_app(app)
    database.init_app(app)
    migrations.init_app(app)
    jwt.init_app(app)
    init_blueprints.init_app(app)

    return app
