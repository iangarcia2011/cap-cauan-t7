from flask import current_app, request, jsonify
from flask_jwt_extended import create_access_token, jwt_required, get_jwt_identity

from sqlalchemy.exc import NoResultFound , InvalidRequestError, IntegrityError
from sqlalchemy.engine.row import Row

from app.models.service_model import ServiceModel
from app.models.requester_model import RequesterModel
from app.models.freelancer_model import FreelancerModel
from app.models.candidates_model import CandidatesModel

from app.configs.database import db

@jwt_required()
def create_service():
    data_json = request.get_json()


    if not data_json.get('description'):
        return {'msg': ' It is not allowed to create the service with an empty description! '}, 400

    try:
        is_requester:RequesterModel = RequesterModel.query.filter_by(email=get_jwt_identity()).one()

        data_json["status_id"] = 1
        data_json["requester_id"] = is_requester.id

        service = ServiceModel(**data_json)

        session = current_app.db.session
        session.add(service)
        session.commit()

        return jsonify(service), 201
    except NoResultFound:
        return {'msg': f'{get_jwt_identity()} não é requester!'}, 404
    except TypeError as e:
        return {'msg': str(e)}, 400


@jwt_required()
def get_list_service():
    data = dict(request.args)

    try:
        if not data:
            list_services = ServiceModel.query.all()
        else:
            list_services = ServiceModel.query.filter_by(**data).all()

        return jsonify(list_services)
    except InvalidRequestError:
        return {'msg': 'Invalid arguments!'}, 400


@jwt_required()
def specific_service(service_id):

    service = ServiceModel.query.get(service_id)

    return jsonify(service), 200


@jwt_required()
def filter_service():
    data = request.data
    print(data)
    
    return jsonify(''), 200



@jwt_required()
def update_service(service_id):

    email = get_jwt_identity()
    data_json = request.get_json()

    if 'requester_id' in data_json:
        return {'message': 'is not allowed change requester_id in this end-point'}, 406

    requester_id = RequesterModel.query.filter_by(email=email).first().id
    update_service = ServiceModel.query.get(service_id)

    if not update_service:
        return {'msg': 'this living does not exist!'}, 404
    
    if data_json.get('chosen_freelancer_id') :
        update_service.status_id = 2

    if requester_id == update_service.requester_id:
        for key, value in data_json.items():
            setattr(update_service, key, value)

        session = current_app.db.session
        session.add(update_service)
        session.commit()

        return jsonify(update_service), 200
    return {'msg': "You do not own this service!"}, 404



@jwt_required()
def delete_service(service_id):
    user_email = get_jwt_identity()
   
    requester_id = RequesterModel.query.filter_by(email=user_email).first().id
    service = ServiceModel.query.get(service_id)

    try:
        if requester_id == service.requester_id:

            list_candidates = CandidatesModel.query.filter_by(service_id= service_id).all()
            session = current_app.db.session

            for i in list_candidates:
                session.delete(i)

            session.delete(service)
            session.commit()

            return "", 200
    
        return {'msg': "You do not own this service!"}, 404
    except AttributeError:
        return {'msg': 'Do not service it with this id!'}

# 



@jwt_required()
def apply_for_the_service(service_id):
    try:
    
        is_freelancer:FreelancerModel = FreelancerModel.query.filter_by(email=get_jwt_identity()).one()

        
        candidate = CandidatesModel(service_id= service_id, freelancer_id= is_freelancer.id)

        session = current_app.db.session
        session.add(candidate)
        session.commit()

        return {'msg': 'application made!'}, 201

    except NoResultFound:
        return {'msg': 'Your account cannot apply for the service!'}, 404
    
    except IntegrityError:
        return {'msg': 'This service does not exist'}, 404


@jwt_required()
def get_all_candidates(service_id):
    
    query:Row = db.session.query(ServiceModel, FreelancerModel).\
            select_from(ServiceModel).\
            join(CandidatesModel).\
            join(FreelancerModel).\
            filter(CandidatesModel.service_id == service_id).all()
    

    list_candidates = [list(o)[1] for o in query]

    return jsonify(list_candidates), 200

