from flask_jwt_extended.utils import get_jwt_identity
from flask.json import jsonify
from app.models.requester_model import RequesterModel
from flask import current_app, request
from flask_jwt_extended import create_access_token, jwt_required
from sqlalchemy.exc import IntegrityError

def create_requester():
    data = request.json
    try:
        requester = RequesterModel(**data)
        
        session = current_app.db.session
        session.add(requester)
        session.commit()

        return  jsonify(requester), 201

    except IntegrityError:
        return {'message': 'This account already exists.'}, 409


def requester_login():

    user_email = request.json.get('email')
    user_password = request.json.get('password')

    requester = RequesterModel.query.filter_by(email=user_email).first()

    if not requester or not requester.verify_password(user_password):
        return {"message": "Wrong email or password."}, 401
            
    access_token = create_access_token(requester.email)
    return {"access_token": access_token}, 200


@jwt_required()
def get_requester_info():

    email = get_jwt_identity()

    requester = RequesterModel.query.filter_by(email=email).first()
    
    if not requester:
        return {"message": "This account does not exist."}, 404

    return jsonify(requester), 200


def requesters_list():
    
    all_requesters = RequesterModel.query.all()

    return jsonify(all_requesters), 200


@jwt_required()
def update_requester():

    email = get_jwt_identity()
    data = request.get_json()

    if 'email' in data:
        return {'message': 'Is not allowed change email in this end-point.'}, 406


    updated_user = RequesterModel.query.filter_by(email=email).first()

    if not updated_user:
        return {"message": "This account does not exist."}, 404
    
    for key, value in data.items():
        setattr(updated_user, key, value)
        
    session = current_app.db.session
    session.add(updated_user)
    session.commit()

    return jsonify(updated_user), 200


@jwt_required()
def delete_requester_account():
    user_email = get_jwt_identity()

    try:

        requester = RequesterModel.query.filter_by(email=user_email).first()
        session = current_app.db.session
        session.delete(requester)
        session.commit()

        return "", 200


    except IntegrityError:
        return {'msg': 'It is not possible to delete this account'}, 400