from app.models.freelancer_model import FreelancerModel
from flask import current_app, jsonify, request
from flask_jwt_extended import create_access_token, get_jwt_identity, jwt_required
from sqlalchemy.exc import IntegrityError


def create_freelancer():
    client_request = request.json
    try:
        new_freelancer = FreelancerModel(**client_request)

        session = current_app.db.session
        session.add(new_freelancer)
        session.commit()

        return jsonify(new_freelancer), 201
    
    except IntegrityError:
            return {'message': 'This account already exists.'}, 409


def freelancer_login():

    client_email    = request.json.get('email')
    client_password = request.json.get('password')

    supposed_client = FreelancerModel.query.filter_by(email=client_email).first()

    if not supposed_client or not supposed_client.verify_password(client_password):
        return {'message': 'Wrong email or password.'}, 404

    access_token = create_access_token(supposed_client.email) 
    
    return jsonify({'access_token': access_token}), 200 
    

@jwt_required()
def get_freelancer_info():

    user_email = get_jwt_identity()    
    current_user = FreelancerModel.query.filter_by(email=user_email).first()

    if not current_user:
            return {"message": "This account does not exist."}, 404

    return jsonify(current_user), 200


def freelancers_list():

    all_freelancers = FreelancerModel.query.all()

    return jsonify(all_freelancers), 200


@jwt_required()
def freelancer_update_info():

    email = get_jwt_identity()
    client_request = request.get_json()

    if 'email' in client_request:
        return {'message': 'Is not allowed to change email in this end-point.'}, 406

    updated_user = FreelancerModel.query.filter_by(email=email).first()

    if not updated_user:
        return {"message": "This account does not exist."}, 404

    for key, value in client_request.items():
        setattr(updated_user, key, value)

    session = current_app.db.session
    session.add(updated_user)
    session.commit()

    return jsonify(updated_user), 200


@jwt_required()
def delete_freelancer_account():

    email = get_jwt_identity()
    
    try:
        current_user = FreelancerModel.query.filter_by(email=email).first()

        session = current_app.db.session
        session.delete(current_user)
        session.commit()

        return "", 204

    except IntegrityError:
        return {'msg': 'It is not possible to delete this account'}, 400



