from flask                       import jsonify, request, current_app
from app.models.categories_model import CategoriesModel
from flask_jwt_extended          import jwt_required
from sqlalchemy.exc              import IntegrityError

@jwt_required()
def create_category():
    try:

        client_request = request.json
        new_category = CategoriesModel(**client_request)

        session = current_app.db.session
        session.add(new_category)

        session.commit()


        return jsonify(new_category), 201

    except IntegrityError:
        return {'message': 'this type already exists'}, 409
    
    except TypeError as e:
        return {'msg': str(e)}, 400


def get_categories_list():

    all_categories = CategoriesModel.query.all()

    return jsonify(all_categories), 200

