from flask              import Blueprint
from app.controllers    import freelancer_controller


bp_freelancer = Blueprint('bp_freelancer', __name__, url_prefix='/freelancer')


bp_freelancer.post('/signup')(freelancer_controller.create_freelancer)
bp_freelancer.post('/login')(freelancer_controller.freelancer_login)
bp_freelancer.get('')(freelancer_controller.get_freelancer_info)
bp_freelancer.get('/list')(freelancer_controller.freelancers_list)
bp_freelancer.patch('')(freelancer_controller.freelancer_update_info)
bp_freelancer.delete('')(freelancer_controller.delete_freelancer_account)



