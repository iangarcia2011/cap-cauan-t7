from flask           import Blueprint
from app.controllers import categories_controller

bp_categories = Blueprint('categories_routes', __name__, url_prefix='/categories')

bp_categories.post('/create')(categories_controller.create_category)
bp_categories.get('/list')(categories_controller.get_categories_list)

