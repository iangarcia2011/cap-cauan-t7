from flask import Flask


def init_app(app: Flask):
 
    from .freelancer_blueprint import bp_freelancer
    app.register_blueprint(bp_freelancer)

    from .requester_blueprint import bp_requester
    app.register_blueprint(bp_requester)

    from .service_blueprint import bp_service
    app.register_blueprint(bp_service)

    # from .status_service_blueprint import
    # app.register_blueprint()

    from .categories_blueprint import bp_categories
    app.register_blueprint(bp_categories)

    # from .candidates_blueprint import
    # app.register_blueprint()
    ...
