from flask import Blueprint
from app.controllers.service_controller import  create_service,\
                                                get_list_service,\
                                                apply_for_the_service,\
                                                get_all_candidates,\
                                                specific_service,\
                                                update_service,\
                                                delete_service

bp_service = Blueprint("bp_service", __name__, url_prefix="/service")

bp_service.post('')(create_service)
bp_service.get('')(get_list_service)
bp_service.get('/<service_id>')(specific_service)
bp_service.patch('/<service_id>')(update_service)
bp_service.delete('/<service_id>')(delete_service)

bp_service.post('/candidate/<service_id>')(apply_for_the_service)
bp_service.get('/candidates/<service_id>')(get_all_candidates)
