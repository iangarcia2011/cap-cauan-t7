from flask import Blueprint
from app.controllers.requester_controller import create_requester, delete_requester_account, get_requester_info, requester_login, requesters_list, update_requester

bp_requester = Blueprint("bp_requester", __name__, url_prefix="/requester")

bp_requester.post('/signup')(create_requester)
bp_requester.post('/login')(requester_login)
bp_requester.get('')(get_requester_info)
bp_requester.get('/list')(requesters_list)
bp_requester.patch('')(update_requester)
bp_requester.delete('')(delete_requester_account)