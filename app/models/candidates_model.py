from sqlalchemy             import Column, Integer, ForeignKey
from app.configs.database   import db
from dataclasses            import dataclass

@dataclass
class CandidatesModel(db.Model):

    id:             int
    service_id:     int
    freelancer_id:  int

    __tablename__ = 'candidates'

    id              = Column(Integer, primary_key=True)
    service_id      = Column(Integer, ForeignKey('service.id'), nullable=False)
    freelancer_id   = Column(Integer, ForeignKey('freelancer.id'))

 