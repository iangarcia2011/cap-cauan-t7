from dataclasses import dataclass
from datetime import date

from app.configs.database import db
from sqlalchemy import Column, Date, Integer, String
from sqlalchemy.orm import relationship
from werkzeug.security import check_password_hash, generate_password_hash


@dataclass
class RequesterModel(db.Model):
 
    id: int
    name: str
    birthdate: date
    email: str

    __tablename__ = 'requester'

    id = Column(Integer, primary_key=True)
    name = Column(String(128), nullable=False)
    birthdate = Column(Date)
    email = Column(String(255), nullable=False, unique=True)
    password_hash = Column(String(255), nullable=False)
    
    services = relationship('ServiceModel', backref='requester')


    @property
    def password(self):
        raise AttributeError("Password cannot be accessed!")

    @password.setter
    def password(self, password_to_hash):
        self.password_hash = generate_password_hash(password_to_hash)

    def verify_password(self, password_to_compare):
        return check_password_hash(self.password_hash, password_to_compare)

    def update_user(self, data):
        
        self.name = data['name']
        self.birthdate = data['birthdate']
        # self.email = data['email']
        self.password = data['password']
