from sqlalchemy import Column, Text, Integer, ForeignKey
from app.configs.database import db
from dataclasses import dataclass

@dataclass
class CommentsModel(db.Model):
    id:             int
    comment:        str

    __tablename__ = 'comments'

    id          = Column(Integer, primary_key=True)
    comment     = Column(Text)
    services_id = Column(Integer, ForeignKey('service.id'), nullable=False)

 
