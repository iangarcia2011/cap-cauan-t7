from dataclasses import dataclass

from app.configs.database import db
from sqlalchemy import Column, Integer, String
from sqlalchemy.orm import relationship


@dataclass
class CategoriesModel(db.Model):
    id: int
    types: str
 
    __tablename__ = 'categories'

    id = Column(Integer, primary_key=True)
    types = Column(String(128), nullable=False, unique=True)


