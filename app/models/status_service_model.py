from sqlalchemy             import Column, String, Integer
from dataclasses            import dataclass
from app.configs.database   import db

@dataclass
class StatusServiceModel(db.Model):

    status_type:    str
    
    __tablename__ = 'status'

    id          = Column(Integer, primary_key=True)
    status_type = Column(String, nullable=False)

 
