from sqlalchemy import Column, Integer, String, DateTime
from sqlalchemy.sql.schema import ForeignKey
from sqlalchemy.orm import backref, relationship
from dataclasses import dataclass
from werkzeug.security import generate_password_hash, check_password_hash

from app.configs.database import db


@dataclass
class FreelancerModel(db.Model):
 
    id: int
    name: str
    birthdate: str
    email: str
    about: str
    categories: dict

    __tablename__ = 'freelancer'

    id = Column(Integer, primary_key=True)
    name = Column(String(127), nullable=False)
    birthdate = Column(DateTime)
    email = Column(String(127), nullable=False, unique=True)
    password_hash = Column(String(127), nullable=False)
    about = Column(String(511))
    categories_id = Column(Integer, ForeignKey('categories.id'))

    services = relationship('ServiceModel', backref="freelancer")  
    categories = relationship('CategoriesModel', backref='freelancer')

# status = relationship('StatusServiceModel', backref='service')
    @property
    def password(self):
        raise AttributeError("Password cannot be accessed!")


    @password.setter
    def password(self, password_to_hash):
        self.password_hash = generate_password_hash(password_to_hash)


    def verify_password(self, password_to_compare):
        return check_password_hash(self.password_hash, password_to_compare)
