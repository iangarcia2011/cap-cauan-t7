from dataclasses import dataclass
from datetime import datetime

from app.configs.database import db
from sqlalchemy import Column, Integer, String
from sqlalchemy.orm import relationship
from sqlalchemy.sql.schema import ForeignKey
from sqlalchemy.sql.sqltypes import DateTime, Float


@dataclass
class ServiceModel(db.Model):
 
    id: int
    description: str
    service_date: datetime
    budget: float
    rating: int
    requester_id: int
    chosen_freelancer_id: int
    status: dict
    categories: dict

    __tablename__ = 'service'

    id = Column(Integer, primary_key=True)
    description = Column(String(550))
    service_date = Column(DateTime, default=datetime.now())
    budget = Column(Float)
    rating = Column(Integer)
    chosen_freelancer_id = Column(Integer, ForeignKey('freelancer.id'))
    categories_id = Column(Integer, ForeignKey('categories.id'))
    requester_id = Column(Integer, ForeignKey('requester.id'), nullable=False)
    status_id = Column(Integer, ForeignKey('status.id'))

    candidates = relationship('CandidatesModel', backref='service')
    status = relationship('StatusServiceModel', backref='service')

    categories = relationship('CategoriesModel', backref='service')